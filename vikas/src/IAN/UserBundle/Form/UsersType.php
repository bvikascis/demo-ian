<?php

namespace IAN\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsersType extends AbstractType
{
    
    /**	
    * @param FormBuilderInterface $builder
    * @param array $options
    */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder->add('name', 'text', 

                array(
                        'label' => 'Name',
                    
                        'attr' => array(
                            'placeholder' => 'Please enter Name',
                        ),
                        'constraints' =>  new NotBlank(),
                        'horizontal_input_wrapper_class' => 'col-lg-6',
                    )

                );
	
	$builder->add('gender', 'choice',

                array(
                        'label' => 'Gender',
			'multiple'     => false,
			'expanded'     => true,
			'choices'      => array('1' => 'Male', '2' => 'Femal'),
			'data' => 1,
			'widget_type'  => "inline",
                        'attr' => array(
                            'placeholder' => 'Please enter Gender',
                        ),
                        'constraints' =>  new NotBlank(),
                        'horizontal_input_wrapper_class' => 'col-lg-6',
                    )

                );
	
	$builder->add('email', 'text',

                array(
                        'label' => 'Email',
                    
                        'attr' => array(
                            'placeholder' => 'Please enter Email Id',
                        ),
                        'constraints' =>  new NotBlank(),
                        'horizontal_input_wrapper_class' => 'col-lg-6',
                    )

                );
	
	$builder->add('password', 'password', 

                array(
                        'label' => 'Password',
                    
                        'attr' => array(
                            'placeholder' => 'Please enter Password',
                        ),
                        'constraints' =>  new NotBlank(),
                        'horizontal_input_wrapper_class' => 'col-lg-6',
                    )

                );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IAN\UserBundle\Entity\Users'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ian_userbundle_users';
    }
}
