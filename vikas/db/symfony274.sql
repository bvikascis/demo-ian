-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 25, 2015 at 10:49 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony274`
--

-- --------------------------------------------------------
--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gender` tinyint(1) NOT NULL COMMENT '1=> Male, 2=> Female',
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `gender`, `email`, `password`) VALUES
(1, 'Vikas', 1, 'vikas.b@cisinlabs.com', '$2y$15$hGzqqS2QIVc1kxM7HPOFiuDVY51rgsbIA3rT3lUaMYp9oytrcguFS'),
(2, 'Pushpendra', 1, 'pushpendra.c@cisinlabs.com', '$2y$15$BLwym.PVcvLMbcrxs7iO2u2WeNl5kREuWdoSdk93TLJiapDm3CQpS'),
(3, 'Krishna Panchal', 1, 'krishna.p@cisinlabs.com', '$2y$15$u24zkw4sl84th4xfWgYty.idHthCuGY4A9OOW1.9JPvK1LDkABGhG'),
(4, 'jatin', 1, 'jatin.b@cisinlabs.com', '$2y$15$83vEq.mQ1x4inamJ3reL5OJt3URuq.QVBBSHWB0uJX7yOn2Orv5A6');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
